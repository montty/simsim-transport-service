package vnd.simsimtransport.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vnd.simsimtransport.model.Credentials;
import vnd.simsimtransport.model.CredentialsRepository;
import vnd.simsimtransport.model.UserDataRepository;
import vnd.simsimtransport.model.vehicle.QVehicle;
import vnd.simsimtransport.model.vehicle.Vehicle;
import vnd.simsimtransport.model.vehicle.VehicleRepository;
import vnd.simsimtransport.model.vehicle.VehicleService;
import vnd.simsimtransport.model.world.WorldService;
import vnd.simsimtransport.service.IMainService;
import vnd.simsimtransport.utils.ResponseUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/vehicles")
public class VehicleController {
    private final VehicleRepository vehicleRepository;
    private final VehicleService vehicleService;
    private final WorldService worldService;
    private final CredentialsRepository credentialsRepository;
    private final UserDataRepository userDataRepository;
    private final IMainService mainService;
    private final ObjectMapper objectMapper;
    private final static Logger log = LoggerFactory.getLogger(VehicleController.class);

    @Autowired
    public VehicleController(VehicleRepository vehicleRepository,
                             VehicleService vehicleService,
                             WorldService worldService,
                             CredentialsRepository credentialsRepository,
                             UserDataRepository userDataRepository,
                             IMainService mainService,
                             ObjectMapper objectMapper) {
        this.vehicleRepository = vehicleRepository;
        this.vehicleService = vehicleService;
        this.worldService = worldService;
        this.credentialsRepository = credentialsRepository;
        this.userDataRepository = userDataRepository;
        this.mainService = mainService;
        this.objectMapper = objectMapper;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Page<Vehicle> getVehicles(@QuerydslPredicate(root = Vehicle.class) Predicate predicate,
                                     @RequestParam(required = false, defaultValue = "false") boolean unassigned,
                                     @PageableDefault(sort = "created", direction = Sort.Direction.DESC, size = 10, page = 0) Pageable pageable,
                                     Principal principal,
                                     HttpServletResponse response) throws IOException {

        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            if (unassigned) {
                BooleanExpression expression = QVehicle.vehicle.world.isNull();
                BooleanBuilder builder = new BooleanBuilder();
                builder.and(predicate);
                builder.and(expression);
                return vehicleRepository.findAll(builder, pageable);
            } else {
                return vehicleRepository.findAll(predicate, pageable);
            }
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Vehicle getVehicle(
            @PathVariable("id") String id,
            Principal principal,
            HttpServletResponse response) throws IOException {

        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            return vehicleRepository.findOne(id);
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeVehicle(
            @PathVariable("id") String id,
            Principal principal,
            HttpServletResponse response) throws IOException {

        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            Vehicle vehicle = vehicleRepository.findOne(id);
            if (Objects.nonNull(vehicle)) {
                if (Objects.nonNull(vehicle.getWorld())) {
                    worldService.removeVehicle(vehicle);
                }
                vehicleRepository.delete(id);
            }
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Vehicle createVehicle(Principal principal,
                                 HttpServletResponse response) throws IOException {

        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            return vehicleService.generate(principal.getName());
        }
        return null;
    }
}
