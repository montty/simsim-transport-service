package vnd.simsimtransport.controller;

import com.mysema.query.types.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vnd.simsimtransport.model.Credentials;
import vnd.simsimtransport.model.CredentialsRepository;
import vnd.simsimtransport.model.vehicle.Vehicle;
import vnd.simsimtransport.model.world.Cell;
import vnd.simsimtransport.model.world.World;
import vnd.simsimtransport.model.world.WorldRepository;
import vnd.simsimtransport.model.world.WorldService;
import vnd.simsimtransport.utils.ResponseUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

@Controller
@RequestMapping("/worlds")
public class WorldController {
    private final WorldRepository worldRepository;
    private final WorldService worldService;
    private final CredentialsRepository credentialsRepository;
    private final static Logger log = LoggerFactory.getLogger(WorldController.class);

    @Autowired
    public WorldController(WorldRepository worldRepository,
                           WorldService worldService,
                           CredentialsRepository credentialsRepository) {
        this.worldRepository = worldRepository;
        this.worldService = worldService;
        this.credentialsRepository = credentialsRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Page<World> getWorlds(@QuerydslPredicate(root = World.class) Predicate predicate,
                                 @RequestParam(value = "creator", required = false) String creator,
                                 @RequestParam(value = "created", required = false) String created,
                                 @PageableDefault(sort = "created", direction = Sort.Direction.DESC, size = 5, page = 0) Pageable pageable,
                                 Principal principal,
                                 HttpServletResponse response) throws IOException {

        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            Page<World> page = worldRepository.findAll(predicate, pageable);
            page.forEach(world -> world.setCells(null));
            return page;
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public World getWorld(
            @PathVariable("id") String id,
            @RequestParam(required = false, defaultValue = "true") boolean cells,
            Principal principal,
            HttpServletResponse response) throws IOException {

        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            World world = worldRepository.findOne(id);
            if (!cells) {
                world.setCells(null);
            }
            return world;
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/image")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public void getImage(
            @PathVariable("id") String id,
            HttpServletResponse response) throws IOException {
        World world = worldRepository.findOne(id);
        if (Objects.isNull(world)) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        response.setContentType(MediaType.IMAGE_PNG_VALUE);
        response.getOutputStream().write(WorldService.getImage(world));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeWorld(
            @PathVariable("id") String id,
            Principal principal,
            HttpServletResponse response) throws IOException {
        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            worldRepository.delete(id);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public World createWorld(Principal principal,
                             HttpServletResponse response) throws IOException {
        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            return worldService.generate(19 + new Random().nextInt(21), principal.getName());
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/vehicles")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public World saveVehicles(Principal principal,
                              @PathVariable String id,
                              @RequestBody Set<Vehicle> vehicles,
                              HttpServletResponse response) throws IOException {
        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            World world = worldRepository.findOne(id);
            if (Objects.isNull(world)) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "World is not found");
                return null;
            }
            return worldService.saveVehicles(world, vehicles);
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/stops")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public World addStop(Principal principal,
                         @PathVariable String id,
                         @RequestBody Cell cell,
                         HttpServletResponse response) throws IOException {
        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            World world = worldRepository.findOne(id);
            if (Objects.isNull(world)) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "World is not found");
                return null;
            }
            return worldService.addStop(world, cell);
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/buildings")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public World addBuilding(Principal principal,
                             @PathVariable String id,
                             @RequestBody Cell cell,
                             HttpServletResponse response) throws IOException {
        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            World world = worldRepository.findOne(id);
            if (Objects.isNull(world)) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "World is not found");
                return null;
            }
            return worldService.addBuilding(world, cell);
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/clear")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public World clear(Principal principal,
                       @PathVariable String id,
                       @RequestBody Cell cell,
                       HttpServletResponse response) throws IOException {
        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            World world = worldRepository.findOne(id);
            if (Objects.isNull(world)) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "World is not found");
                return null;
            }
            return worldService.clear(world, cell);
        }
        return null;
    }

}
