package vnd.simsimtransport.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vnd.simsimtransport.model.*;
import vnd.simsimtransport.model.settings.Settings;
import vnd.simsimtransport.model.world.WorldRepository;
import vnd.simsimtransport.model.world.WorldService;
import vnd.simsimtransport.service.IMainService;
import vnd.simsimtransport.utils.ResponseUtils;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;
import java.util.Objects;

@Controller
@RequestMapping("/settings")
public class SettingsController {
    private WorldRepository worldRepository;
    private WorldService worldService;
    private CredentialsRepository credentialsRepository;
    private UserDataRepository userDataRepository;
    private IMainService mainService;
    private ObjectMapper objectMapper;
    private final static Logger log = LoggerFactory.getLogger(SettingsController.class);

    @Autowired
    public SettingsController(WorldRepository worldRepository,
                              WorldService worldService,
                              CredentialsRepository credentialsRepository,
                              UserDataRepository userDataRepository,
                              IMainService mainService,
                              ObjectMapper objectMapper) {
        this.worldRepository = worldRepository;
        this.worldService = worldService;
        this.credentialsRepository = credentialsRepository;
        this.userDataRepository = userDataRepository;
        this.mainService = mainService;
        this.objectMapper = objectMapper;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public UserData save(
            @RequestBody Settings settings,
            Principal principal,
            HttpSession httpSession,
            HttpServletResponse response) throws IOException {
        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            UserData userData = (UserData) httpSession.getAttribute(Constants.USER_DATA);
            userData.setSettings(settings);
            userData = userDataRepository.save(userData);
            httpSession.setAttribute(Constants.USER_DATA, userData);
            return userData;
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public UserData get(
            Principal principal,
            HttpServletResponse response) throws IOException {
        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            return userDataRepository.findOne(credentialsFromDatabase.getId());
        }
        return null;
    }
}
