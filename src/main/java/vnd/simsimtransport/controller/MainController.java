package vnd.simsimtransport.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vnd.simsimtransport.model.*;
import vnd.simsimtransport.utils.DataUtils;
import vnd.simsimtransport.utils.ResponseUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.Objects;

@Controller
@RequestMapping("/")
public class MainController {
    //    private WorldRepository worldRepository;
//    private WorldService worldService;
    private CredentialsRepository credentialsRepository;
    private UserDataRepository userDataRepository;
    //    private IMainService mainService;
//    private ObjectMapper objectMapper;
    private final static Logger log = LoggerFactory.getLogger(MainController.class);

    @Autowired
    public MainController(
//            WorldRepository worldRepository,
//            WorldService worldService,
            CredentialsRepository credentialsRepository,
            UserDataRepository userDataRepository
//            IMainService mainService,
//            ObjectMapper objectMapper
    ) {
//        this.worldRepository = worldRepository;
//        this.worldService = worldService;
        this.credentialsRepository = credentialsRepository;
        this.userDataRepository = userDataRepository;
//        this.mainService = mainService;
//        this.objectMapper = objectMapper;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public UserData getUserData(Principal principal,
                                HttpSession httpSession,
                                HttpServletRequest request,
                                HttpServletResponse response) throws IOException {
        Credentials credentialsFromDatabase = ResponseUtils.process(credentialsRepository, response, principal);
        if (Objects.nonNull(credentialsFromDatabase)) {
            return (UserData) httpSession.getAttribute(Constants.USER_DATA);
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/login", params = "login")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public void login1(@RequestParam String login,
                       HttpServletRequest request,
                       HttpServletResponse response) throws IOException {

        Credentials credentialsFromDatabase = credentialsRepository.findOneByLogin(login);

        if (Objects.isNull(credentialsFromDatabase)) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "User not found");
            return;
        }

        HttpSession httpSession = request.getSession(false);
        if (Objects.nonNull(httpSession)) {
            httpSession.invalidate();
        }
        httpSession = request.getSession(true);
        httpSession.setAttribute(Constants.PRINCIPAL, login);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/login")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public UserData login2(@RequestBody @Valid LoginRequest loginRequest,
                           Principal principal,
                           HttpSession httpSession,
                           HttpServletRequest request,
                           HttpServletResponse response) throws IOException {

        if (Objects.isNull(principal) || StringUtils.isBlank(principal.getName()) || httpSession.isNew()) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "Username hasn't been set yet");
            return null;
        }

        Credentials credentialsFromDatabase = credentialsRepository.findOneByLogin(principal.getName());

        if (Objects.isNull(credentialsFromDatabase)) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "User not found");
            return null;
        }

        if (!loginRequest.getPassword().equals(credentialsFromDatabase.getPassword())) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Wrong password");
            return null;
        }

        UserData userData = userDataRepository.findOne(credentialsFromDatabase.getId());

        if (Objects.isNull(userData)) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }

        httpSession.setAttribute(Constants.USER_DATA, userData);
        return userData;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/legacy-login")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public UserData legacyLogin(@RequestBody @Valid Credentials credentials,
                                HttpServletRequest request,
                                HttpServletResponse response) throws IOException {

        Credentials credentialsFromDatabase = credentialsRepository.findOneByLogin(credentials.getLogin());

        if (Objects.isNull(credentialsFromDatabase)) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "User not found");
            return null;
        }

        String password = DataUtils.hashedPassword(credentials.getPassword());

        if (!password.equals(credentialsFromDatabase.getPassword())) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Wrong password");
            return null;
        }

        HttpSession httpSession = request.getSession(false);
        if (Objects.nonNull(httpSession)) {
            httpSession.invalidate();
        }
        httpSession = request.getSession(true);
        UserData userData = userDataRepository.findOne(credentialsFromDatabase.getId());

        if (Objects.isNull(userData)) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
        httpSession.setAttribute(Constants.PRINCIPAL, credentials.getLogin());
        httpSession.setAttribute(Constants.USER_DATA, userData);
        return userData;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/register")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public UserData login(@RequestBody @Valid RegisterData registerData,
                          HttpServletRequest request,
                          HttpServletResponse response) throws IOException {
        Credentials credentials = new Credentials();
        credentials.setLogin(registerData.getLogin());
        credentials.setPassword(registerData.getPassword());

        Credentials credentialsFromDatabase = credentialsRepository.findOneByLogin(credentials.getLogin());

        if (Objects.nonNull(credentialsFromDatabase)) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "User exists");
            return null;
        }

        credentialsRepository.save(credentials);

        UserData userData = new UserData(credentials.getId(), registerData.getRole(), credentials.getLogin());
        userDataRepository.save(userData);
        HttpSession httpSession = request.getSession(false);
        if (Objects.nonNull(httpSession)) {
            httpSession.invalidate();
        }
        httpSession = request.getSession(true);
        httpSession.setAttribute(Constants.USER_DATA, userData);
        httpSession.setAttribute(Constants.PRINCIPAL, userData.getLogin());

        return userData;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/logout")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public void logout(HttpSession session) {
        session.invalidate();
    }
}
