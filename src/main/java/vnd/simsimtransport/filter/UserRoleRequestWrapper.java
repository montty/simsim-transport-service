package vnd.simsimtransport.filter;

import com.sun.security.auth.UserPrincipal;
import vnd.simsimtransport.model.Constants;
import vnd.simsimtransport.model.UserData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class UserRoleRequestWrapper extends HttpServletRequestWrapper {

    private UserPrincipal principal;
    private List<String> roles;

    public UserRoleRequestWrapper(HttpServletRequest request) {
        super(request);
        HttpSession session = request.getSession(false);
        if (Objects.nonNull(session)) {
            UserData userData = (UserData) session.getAttribute(Constants.USER_DATA);
            if (Objects.nonNull(userData) && Objects.nonNull(userData.getRole())) {
                roles = Arrays.asList(userData.getRole().name());
            }
            String login = (String) session.getAttribute(Constants.PRINCIPAL);
            if (Objects.nonNull(login)) {
                principal = new UserPrincipal(login);
            }
        }
    }

    @Override
    public boolean isUserInRole(String role) {
        return roles.contains(role);
    }

    @Override
    public Principal getUserPrincipal() {
        return principal;
    }
}