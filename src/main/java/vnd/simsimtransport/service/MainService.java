package vnd.simsimtransport.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vnd.simsimtransport.model.MessageRepository;

import javax.annotation.PostConstruct;

@Service
public class MainService implements IMainService {
    private static final Logger log = LoggerFactory.getLogger(MainService.class);

    private final MessageRepository messageRepository;
    private final ObjectMapper objectMapper;

    @Autowired
    public MainService(MessageRepository messageRepository,
                       ObjectMapper objectMapper) {
        this.messageRepository = messageRepository;
        this.objectMapper = objectMapper;
    }

    @PostConstruct
    private void init() {
    }
}
