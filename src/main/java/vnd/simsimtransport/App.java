package vnd.simsimtransport;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import vnd.simsimtransport.utils.DataUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

@SpringBootApplication
@PropertySource("classpath:common.properties")
@EnableAutoConfiguration(exclude = {HibernateJpaAutoConfiguration.class, DataSourceAutoConfiguration.class})
@EnableScheduling
public class App extends SpringBootServletInitializer {

    private static final Logger log = LoggerFactory.getLogger(App.class);
    public static final String SERVER_NAME = createName();

    @PostConstruct
    public void welcome() {
        log.info("[SIMSIMTRANSPORT-I000] Д о б р о  п о ж а л о в а т ь, {}!", SERVER_NAME);
        Thread.setDefaultUncaughtExceptionHandler((t, e) -> log.error("[SIMSIMTRANSPORT-E01]: Исключение в потоке " + t.getName(), e));
    }

    public static void main(String[] args) {
        try {
            File f = new File(System.getProperty("java.io.tmpdir"), "started_" + DataUtils.getIsoDate(new Date()));
            if (!f.exists()) f.mkdirs();
        } catch (RuntimeException e) {
            log.warn("[SIMSIMTRANSPORT-W005]", e);
        }
        SpringApplication.run(App.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(App.class);
    }

//    @Bean
//    public ThreadPoolExecutorFactoryBean executorService(@Value("${threadPoolSize}") Integer threadPoolSize) {
//        ThreadPoolExecutorFactoryBean tpef = new ThreadPoolExecutorFactoryBean();
//        tpef.setCorePoolSize(threadPoolSize);
//        tpef.setQueueCapacity(100);
//        return tpef;
//    }

    private static String createName() {
        String name;
        try {
            name = StringUtils.trimToEmpty(InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException e) {
            log.error("[SIMSIMTRANSPORT-E02]", e);
            name = "Unknown";
        }
        return name;
    }
}
