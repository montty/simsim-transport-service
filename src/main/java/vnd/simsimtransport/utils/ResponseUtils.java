package vnd.simsimtransport.utils;


import vnd.simsimtransport.model.Credentials;
import vnd.simsimtransport.model.CredentialsRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.Objects;

public class ResponseUtils {

    public static Credentials process(CredentialsRepository credentialsRepository, HttpServletResponse response, Principal principal) throws IOException {
        if (Objects.isNull(principal) || Objects.isNull(principal.getName())) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }
        Credentials credentialsFromDatabase = credentialsRepository.findOneByLogin(principal.getName());

        if (Objects.isNull(credentialsFromDatabase)) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "User not found");
            return null;
        }

        return credentialsFromDatabase;
    }

}
