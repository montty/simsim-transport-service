package vnd.simsimtransport.utils;

import java.security.SecureRandom;

/**
 * Генерация всяких красивых названий остановок.
 */
public class RandomStopsUtils {

    private static SecureRandom sr = new SecureRandom();

    private static String[] qualifiers = new String[]{
            "Snowy", "Cloudy", "Sunny", "Rainy", "Platinum", "Copper", "Ivory", "Pale", "Rosy", "Steel", "White", "Green", "Black", "Coral", "Dark", "Light", "Baby", "Lunar", "Sunny", "Martian", "Silver", "Gold", "Bright", "Newborn", "Blue", "Azure", "Strong", "Empire", "Galaxy", "Sky", "Stable", "Everlasting", "Reliable", "Cinema", "Trustworthy", "Straight", "Hot", "Cold", "Ice", "Dicey", "Russian", "Yellow", "Red"};
    private static String[] subject = new String[]{
            "Grove", "Forest", "Field", "Vineyard", "Garden", "Cave", "Village", "Desert", "Star", "Jupiter", "Zion", "Sun", "Mars", "Moon", "Milky Way", "Baby", "Building", "Palace", "Museum", "Square", "Galaxy", "Sky"};

    public static String generate() {
        return qualifiers[sr.nextInt(qualifiers.length)] + " " + subject[sr.nextInt(subject.length)];
    }

}
