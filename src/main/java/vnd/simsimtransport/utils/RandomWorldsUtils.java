package vnd.simsimtransport.utils;

import java.security.SecureRandom;

/**
 * Генерация всяких красивых названий миров.
 */
public class RandomWorldsUtils {

    private static SecureRandom sr = new SecureRandom();

    private static String[] qualifiers = new String[]{
            "", "", "", "World of", "Galaxy of", "Desert of", "Realm of", "Beautilul","Sad", "Hopeless", "Dreadful", "Terrible", "Happy", "Happy", "Encouraging", "Inspiring", "Adventurous", "Snowy", "Cloudy", "Sunny", "Rainy", "Cold", "Icy", "Dicey", "Windy"};
    private static String[] subject = new String[]{
            "Mars", "Moon", "Luna", "Zion", "Venus", "Europa", "Aldebaran", "Desert", "Star", "Jupiter", "Sun", "Milky Way", "Baby Boom", "Valour", "Brothers"};

    public static String generate() {
        return qualifiers[sr.nextInt(qualifiers.length)] + " " + subject[sr.nextInt(subject.length)];
    }

}
