package vnd.simsimtransport.utils;

import java.security.SecureRandom;

/**
 * Генерация всяких красивых названий и чисел
 */
public class RandomUtils {

    private static SecureRandom sr = new SecureRandom();

    private static final String[][] STRINGS_LAT = new String[][]{
            {"Yellow", "Red", "Green", "Azure", "Bisque", "Black", "Blue", "Brown", "Coral", "Crimson", "Dark", "Light", "Gold", "Silver", "Platinum", "Copper", "Ivory", "Pale", "Rosy", "Sky", "Steel", "White"},
            {"Star", "Violet", "Gray", "Purple", "Parrot", "Dog", "Cat", "Cow", "Apple", "Plum", "Rose", "Camomile", "Raccoon", "Jackal", "Wolf", "Hamster", "GuineaPig", "Cockatoo", "Macaw", "Finch", "Pheasant", "Dove", "Sparrow", "Rabbit"},
            {"Dinosaur", "Draco", "Dolphin", "Starfish", "Guppi", "Angelfish", "Carp", "Crucian", "Ramp", "Jellyfish", "Shark", "Whale", "Finwhale", "Tuna"}
    };

    /**
     * Находит новое название,
     *
     * @return {@link String} название
     */
    public static String nextName() {
        return nextName("");
    }

    public static String nextName(String sep) {
        String[][] strings0 = STRINGS_LAT;
        return strings0[0][sr.nextInt(strings0[0].length)] + sep +
                strings0[1][sr.nextInt(strings0[1].length)] +
                (0 == sr.nextInt(2) ? sep + strings0[2][sr.nextInt(strings0[2].length)] : "");
    }

    /**
     * Находит новое ид
     *
     * @return {@link Long} ид
     */
    public static long nextLong() {
        return Math.abs(sr.nextLong());
    }

    /**
     * Находит новое ид (от 0 до максимального значения)
     *
     * @param max максимальное значение
     * @return {@link Long} ид
     */
    public static long nextLong(long max) {
        return Math.abs(sr.nextLong()) % max;
    }

    public static long nextLong(long min, long max) {
        return min + (Math.abs(sr.nextLong()) % (max - min));
    }

    private static final String[] STRINGS_DESC1 = "Хорошiй,Удачный,Сложный,Краснорѣчивый,Хитрый,Изобрѣтательный".split(",");
    private static final String[] STRINGS_DESC2 = "примѣръ,образецъ,учебникъ,смыслъ,умъ,мнѣмоническiй прiемъ,зритѣльный образъ,животный миръ,раститѣльный миръ,анализъ памяти,мнѣмоническiй анализъ".split(",");

    public static String randomDesc() {
        return STRINGS_DESC1[sr.nextInt(STRINGS_DESC1.length)] + " " + STRINGS_DESC2[sr.nextInt(STRINGS_DESC2.length)];
    }

    private static final String[] STRINGS_TITLE1 = "Изученiе,Повторѣнiе,Практика и повторѣнiе,Практика и изученiе,Запоминанiе,Тѣcтированiе".split(",");
    private static final String[] STRINGS_TITLE2 = ("животнаго мира,раститѣльнаго мира,ботаники,чистописанiя,ораторскаго искусства,мѣнеджмента,аналитики," +
            "легкой промышленности,военной промышленности,машиностроенiя будущего,дирижаблѣй,воздушныхъ шаровъ,спуска на парашютѣ съ гондолы,стратосферы," +
            "тропосферы,пѣристыхъ облаковъ,литосферы").split(",");

    public static String randomTitle() {
        return STRINGS_TITLE1[sr.nextInt(STRINGS_TITLE1.length)] + " " + STRINGS_TITLE2[sr.nextInt(STRINGS_TITLE2.length)];
    }

    public static String randomLogin() {
        String[][] strings0 = STRINGS_LAT;
        return strings0[0][sr.nextInt(strings0[0].length)];
    }
}
