package vnd.simsimtransport.utils;


import org.apache.commons.codec.digest.DigestUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class DataUtils {

    private final static String DATE_FORMAT = "dd.MM.yyyy";
    private static AtomicInteger invoiceCount = new AtomicInteger(0);

    public static boolean isDateValid(String date) {
        try {
            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public static String getIsoDate(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public static String adaptPersonalName(String name) {
        return name.replaceAll("[^А-Яа-яA-Za-zЁё\\s\\.\\-']", "");
    }

    public static void setInvoiceCount(AtomicInteger invoiceCount) {
        DataUtils.invoiceCount = invoiceCount;
    }

    public static int increment() {
        return invoiceCount.incrementAndGet();
    }

    /**
     * Извлекает логин из уида.
     *
     * @param uuid уид
     * @return логин
     */
    public static String getLoginByUuid(String uuid) {
        if (null == uuid) return "?";
        return uuid.split(",")[0].split("=")[1].toLowerCase();
    }

    public static String hashedPassword(String password) {
        return DigestUtils.sha256Hex(password);
    }
}
