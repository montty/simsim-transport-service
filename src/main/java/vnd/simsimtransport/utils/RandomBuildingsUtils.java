package vnd.simsimtransport.utils;

import java.security.SecureRandom;

/**
 * Генерация всяких красивых названий зданий.
 */
public class RandomBuildingsUtils {

    private static SecureRandom sr = new SecureRandom();

    private static String[] qualifiers = new String[]{
            "Baby", "Lunar", "Sunny", "Martian", "Silver", "Gold", "Bright", "Newborn", "Blue", "Azure", "Strong", "Empire", "Galaxy", "Sky", "Stable", "Everlasting", "Reliable", "Cinema", "Trustworthy", "Straight", "Hot", "Cold", "Ice", "Dicey", "Russian", "Yellow", "Red"};
    private static String[] subject = new String[]{
            "Jupiter", "Zion", "Sun", "Mars", "Moon", "Milky Way", "Baby", "Building", "Palace", "Museum", "Square", "Galaxy", "Sky", "Skyscraper", "Cathedral", "Theater", "Cinema", "Chapel", "Supermarket", "Shop", "Business Center", "Mall"
    };

    public static String generate() {
        return qualifiers[sr.nextInt(qualifiers.length)] + " " + subject[sr.nextInt(subject.length)];
    }
}
