package vnd.simsimtransport.utils;

import vnd.simsimtransport.model.vehicle.WayType;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Генерация всяких красивых названий видов транспорта.
 */
public class RandomVehiclesUtils {

    private static SecureRandom sr = new SecureRandom();

    private static String[][] qualifiers = new String[][]{
            {"Water"},
            {"Air"},
            {"Ground"},
            {"Underground"},
            {"Platinum", "Copper", "Ivory", "Pale", "Rosy", "Steel", "White", "Green", "Black", "Coral", "Dark", "Light", "Baby", "Lunar", "Sunny", "Martian", "Silver", "Gold", "Bright", "Newborn", "Blue", "Azure", "Strong", "Empire", "Galaxy", "Sky", "Stable", "Everlasting", "Reliable", "Trustworthy", "Straight", "Hot", "Cold", "Ice", "Dicey", "Russian", "Yellow", "Red"}
    };
    private static String[][] subjects = new String[][]{
            {"Dolphin", "Starfish", "Guppi", "Angelfish", "Carp", "Crucian", "Jellyfish", "Shark", "Whale", "Finwhale", "Tuna"},
            {"Cockatoo", "Macaw", "Finch", "Pheasant", "Dove", "Sparrow", "Parrot"},
            {"Ramp", "Raccoon", "Jackal", "Wolf", "Hamster", "GuineaPig", "Rabbit", "Cockatoo", "Macaw", "Finch", "Pheasant", "Dove", "Sparrow", "Parrot", "Dinosaur"},
            {"Mole"},
            {"Draco", "Dinosaur"}
    };

    public static String generate(WayType wayType) {
        List<String> qualifiers = new ArrayList<>();
        qualifiers.addAll(Arrays.asList(RandomVehiclesUtils.qualifiers[wayType.ordinal()]));
        qualifiers.addAll(Arrays.asList(RandomVehiclesUtils.qualifiers[RandomVehiclesUtils.qualifiers.length - 1]));

        List<String> subjects = new ArrayList<>();
        subjects.addAll(Arrays.asList(RandomVehiclesUtils.subjects[wayType.ordinal()]));
        subjects.addAll(Arrays.asList(RandomVehiclesUtils.subjects[RandomVehiclesUtils.subjects.length - 1]));

        return qualifiers.get(sr.nextInt(qualifiers.size())) + " " + subjects.get(sr.nextInt(subjects.size()));
    }


}
