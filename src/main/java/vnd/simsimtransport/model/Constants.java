package vnd.simsimtransport.model;

public class Constants {
    public static final String USER_DATA = "USER_DATA";
    public static final String PRINCIPAL = "PRINCIPAL";

    private Constants() {
    }
}
