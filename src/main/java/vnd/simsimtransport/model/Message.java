package vnd.simsimtransport.model;

import com.mysema.query.annotations.QuerySupertype;

import javax.persistence.Id;
import java.util.Date;

@QuerySupertype
@org.springframework.data.mongodb.core.mapping.Document
//@JsonTypeInfo(defaultImpl = PaymentOrderStatusInfo.class, use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
//@JsonTypeInfo(property = "t", use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY)
//@JsonSubTypes({
//        @JsonSubTypes.CellType(value=PaymentOrderStatusInfo.class, name="RESPONSE"),
//        @JsonSubTypes.CellType(value=PaymentRequestOrderWithInvoice.class, name="REQUEST")
//})
public class Message {

    @Id
    private String id;
    private Date date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
