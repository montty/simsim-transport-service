package vnd.simsimtransport.model;

import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.StringPath;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

/**
 * Репозиторий событий выставления счета
 */
//@RepositoryRestResource
public interface MessageRepository extends MongoRepository<Message, String>, QueryDslPredicateExecutor<Message>, QuerydslBinderCustomizer<QMessage> {
    Page<Message> findAll(Pageable pageable);

    Page<Message> findAll(Predicate predicate, Pageable pageable);

//    @Query(value = "{ 'sha1' : ?0, 'type' : ?1, $or: [{'debugInfo.version' : ?2, 'state': {$ne: 'RECHECK'}}, {'state' : 'MANUAL'}] }")
//    List<TariffEvent> findReady(String sha1, CellType type, String version);
//
//    Message findOneByRequestIdOrderByDateDesc(String requestId);
//
//    long count(Predicate predicate);

    // https://jira.spring.io/browse/DATACMNS-115
    // List<String> findDistinctDebugInfo_Version();

    @Override
    default void customize(QuerydslBindings querydslBindings, QMessage root) {
        querydslBindings.bind(String.class).first((StringPath path, String value) -> path.containsIgnoreCase(value));
    }
}
