package vnd.simsimtransport.model;

import org.hibernate.validator.constraints.NotBlank;
import vnd.simsimtransport.utils.DataUtils;

import javax.validation.constraints.NotNull;

public class RegisterData {
    @NotBlank
    private String login;
    @NotBlank
    private String password;
    @NotNull
    private Role role;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = DataUtils.hashedPassword(password);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
