package vnd.simsimtransport.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QUserData is a Querydsl query type for UserData
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QUserData extends EntityPathBase<UserData> {

    private static final long serialVersionUID = -345469215L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUserData userData = new QUserData("userData");

    public final StringPath id = createString("id");

    public final StringPath login = createString("login");

    public final StringPath name = createString("name");

    public final EnumPath<Role> role = createEnum("role", Role.class);

    public final vnd.simsimtransport.model.settings.QSettings settings;

    public QUserData(String variable) {
        this(UserData.class, forVariable(variable), INITS);
    }

    public QUserData(Path<? extends UserData> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QUserData(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QUserData(PathMetadata<?> metadata, PathInits inits) {
        this(UserData.class, metadata, inits);
    }

    public QUserData(Class<? extends UserData> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.settings = inits.isInitialized("settings") ? new vnd.simsimtransport.model.settings.QSettings(forProperty("settings")) : null;
    }

}

