package vnd.simsimtransport.model;

import org.hibernate.validator.constraints.NotBlank;
import vnd.simsimtransport.utils.DataUtils;

public class LoginRequest {
    @NotBlank
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = DataUtils.hashedPassword(password);
    }
}
