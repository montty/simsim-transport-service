package vnd.simsimtransport.model;

import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.StringPath;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

/**
 * User data repository.
 */
public interface UserDataRepository extends MongoRepository<UserData, String>, QueryDslPredicateExecutor<UserData>, QuerydslBinderCustomizer<QUserData> {

    @Override
    default void customize(QuerydslBindings querydslBindings, QUserData root) {
        querydslBindings.bind(String.class).first((StringPath path, String value) -> path.containsIgnoreCase(value));
    }

}
