package vnd.simsimtransport.model.settings;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QSettings is a Querydsl query type for Settings
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QSettings extends BeanPath<Settings> {

    private static final long serialVersionUID = 2022938242L;

    public static final QSettings settings = new QSettings("settings");

    public final MapPath<vnd.simsimtransport.model.vehicle.WayType, Choice, SimplePath<Choice>> waytypeChoices = this.<vnd.simsimtransport.model.vehicle.WayType, Choice, SimplePath<Choice>>createMap("waytypeChoices", vnd.simsimtransport.model.vehicle.WayType.class, Choice.class, SimplePath.class);

    public final SetPath<vnd.simsimtransport.model.world.World, vnd.simsimtransport.model.world.QWorld> worlds = this.<vnd.simsimtransport.model.world.World, vnd.simsimtransport.model.world.QWorld>createSet("worlds", vnd.simsimtransport.model.world.World.class, vnd.simsimtransport.model.world.QWorld.class, PathInits.DIRECT2);

    public QSettings(String variable) {
        super(Settings.class, forVariable(variable));
    }

    public QSettings(Path<? extends Settings> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSettings(PathMetadata<?> metadata) {
        super(Settings.class, metadata);
    }

}

