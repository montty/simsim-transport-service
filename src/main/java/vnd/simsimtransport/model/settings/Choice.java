package vnd.simsimtransport.model.settings;

public class Choice {
    private boolean selected;

    public Choice() {
    }

    public Choice(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
