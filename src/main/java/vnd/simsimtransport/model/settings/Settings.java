package vnd.simsimtransport.model.settings;

import vnd.simsimtransport.model.vehicle.WayType;
import vnd.simsimtransport.model.world.World;

import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Set;

public class Settings {
    public static final Settings DEFAULT;

    static {
        DEFAULT = new Settings();
        DEFAULT.setWorlds(Collections.emptySet());
        Choice defaultChoice = new Choice(true);
        EnumMap<WayType, Choice> waytypeChoices = new EnumMap<>(WayType.class);
        waytypeChoices.put(WayType.WATER, defaultChoice);
        waytypeChoices.put(WayType.AIR, defaultChoice);
        waytypeChoices.put(WayType.GROUND, defaultChoice);
        waytypeChoices.put(WayType.UNDERGROUND, defaultChoice);
        DEFAULT.setWaytypeChoices(waytypeChoices);
    }

    private EnumMap<WayType, Choice> waytypeChoices;

    private Set<World> worlds;

    public EnumMap<WayType, Choice> getWaytypeChoices() {
        return waytypeChoices;
    }

    public void setWaytypeChoices(EnumMap<WayType, Choice> waytypeChoices) {
        this.waytypeChoices = waytypeChoices;
    }

    public Set<World> getWorlds() {
        return worlds;
    }

    public void setWorlds(Set<World> worlds) {
        this.worlds = worlds;
    }
}
