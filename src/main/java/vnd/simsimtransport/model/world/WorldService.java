package vnd.simsimtransport.model.world;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vnd.simsimtransport.model.vehicle.Vehicle;
import vnd.simsimtransport.model.vehicle.VehicleRepository;
import vnd.simsimtransport.utils.RandomBuildingsUtils;
import vnd.simsimtransport.utils.RandomStopsUtils;
import vnd.simsimtransport.utils.RandomWorldsUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class WorldService {

    private final WorldRepository worldRepository;
    private final VehicleRepository vehicleRepository;

    @Autowired
    public WorldService(WorldRepository worldRepository, VehicleRepository vehicleRepository) {
        this.worldRepository = worldRepository;
        this.vehicleRepository = vehicleRepository;
    }

    public World generate(int measure, String userId) throws IOException {
        World world = new World();
        world.setMeasure(measure);

        BufferedImage ground = generateGroundImage(measure, measure, 2 * measure, 2 * measure, measure / 10);
        BufferedImage subway = generateSubwayImage(measure);

        Cell[][] cells = new Cell[measure][measure];

        for (int i = 0; i < measure; i++) {
            for (int j = 0; j < measure; j++) {
                int color = ground.getRGB(i, j);
                Cell cell = new Cell(i, j, CellType.fromColor(color));

                cell.setUnderground(CellType.SUBWAY.getColor().getRGB() == subway.getRGB(i, j));

                if (CellType.BUILDING.equals(cell.getType())) {
                    cell.setName(RandomBuildingsUtils.generate());
                } else if (CellType.STOP.equals(cell.getType())) {
                    cell.setName(RandomStopsUtils.generate());
                }

                cells[i][j] = cell;

            }
        }
        world.setCells(cells);
        world.setName(RandomWorldsUtils.generate());
        world.setCreator(userId);
        world.setCreated(new Date());
        worldRepository.save(world);
        return world;
    }

    public BufferedImage generateGroundImage(int measure, int stops, int trees, int buildings, int lakes) throws IOException {
        Random random = new Random();

        Color[] colors = Arrays.asList(CellType.values()).stream().map(CellType::getColor).collect(Collectors.toList()).toArray(new Color[CellType.values().length]);

        BufferedImage image = createIndexImage(measure, 4, colors);

        Graphics2D g = image.createGraphics();

        // stops
        g.setColor(CellType.STOP.getColor());
        for (int i = 0; i < stops; i++) {
            int x = random.nextInt(measure);
            int y = random.nextInt(measure);
            g.drawLine(x, y, x, y);
        }

        // trees
        g.setColor(CellType.TREE.getColor());
        for (int i = 0; i < trees; i++) {
            int x = random.nextInt(measure);
            int y = random.nextInt(measure);
            g.drawLine(x, y, x, y);
        }

        g.setColor(CellType.BUILDING.getColor());
        for (int i = 0; i < buildings; i++) {
            int x = random.nextInt(measure);
            int y = random.nextInt(measure);
            g.drawLine(x, y, x, y);
        }

        g.setColor(CellType.WATER.getColor());
        for (int i = 0; i < lakes; i++) {
            g.fillOval(random.nextInt(measure), random.nextInt(measure), random.nextInt(measure / 2), random.nextInt(measure / 2));
        }

        return image;
    }

    public BufferedImage generateSubwayImage(int measure) throws IOException {
        Random random = new Random();

        Color[] colors = Arrays.asList(CellType.values()).stream().map(CellType::getColor).collect(Collectors.toList()).toArray(new Color[CellType.values().length]);

        BufferedImage image = createIndexImage(measure, 4, colors);

        Graphics2D g = image.createGraphics();

        g.setColor(CellType.SUBWAY.getColor());

        BasicStroke stroke = new BasicStroke(1);
        g.setStroke(stroke);
        g.drawRect(random.nextInt(measure), random.nextInt(measure), random.nextInt(measure), random.nextInt(measure));
        return image;
    }

    private static BufferedImage createIndexImage(int measure, int bpp, Color[] colors) {

        // calculate palette size
        int psize = (1 << bpp);

        // prepare palette;
        byte[] r = new byte[psize];
        byte[] g = new byte[psize];
        byte[] b = new byte[psize];

        for (int i = 0; i < colors.length; i++) {
            r[i] = (byte) (0xff & colors[i].getRed());
            g[i] = (byte) (0xff & colors[i].getGreen());
            b[i] = (byte) (0xff & colors[i].getBlue());
        }

        // now prepare appropriate index clor model
        IndexColorModel icm = new IndexColorModel(bpp, psize, r, g, b);
        return new BufferedImage(measure, measure, BufferedImage.TYPE_BYTE_INDEXED, icm);
    }

    public static byte[] getImage(World world) {
        Color[] colors = Arrays.asList(CellType.values()).stream().map(CellType::getColor).collect(Collectors.toList()).toArray(new Color[CellType.values().length]);
        BufferedImage image = new BufferedImage(world.getMeasure(), world.getMeasure(), BufferedImage.TYPE_3BYTE_BGR);
        for (int column = 0; column < world.getMeasure(); column++) {
            for (int row = 0; row < world.getMeasure(); row++) {
                Cell cell = world.getCells()[column][row];
                Color color = cell.getType().getColor();
                if (cell.isUnderground()) {
                    color = underground(color);
                }
                image.setRGB(row, column, color.getRGB());
            }
        }
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(image, "png", baos);
            return baos.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }

    public World saveVehicles(World world, Set<Vehicle> vehicles) {
        World cutDownVersion = world.cutDownVersion();
        List<Vehicle> toRemove = world.getVehicles().stream().filter(v -> !vehicles.stream().anyMatch(u -> u.getId().equals(v.getId()))).collect(Collectors.toList());
        List<Vehicle> toAdd = vehicles.stream().filter(v -> !world.getVehicles().stream().anyMatch(u -> u.getId().equals(v.getId()))).collect(Collectors.toList());

        for (Vehicle vehicle : toAdd) {
            vehicle.setWorld(cutDownVersion);
            vehicleRepository.save(vehicle);
            vehicle.setWorld(null);
        }

        for (Vehicle vehicle : toRemove) {
            vehicle.setWorld(null);
            vehicleRepository.save(vehicle);
        }

        world.setVehicles(vehicles);
        return worldRepository.save(world);
    }

    private static Color underground(Color color) {
        int r = Math.min(255, color.getRed() + 120);
        int g = Math.min(255, color.getGreen() + 120);
        int b = Math.min(255, color.getBlue() + 120);
        return new Color(r, g, b);
    }

    public void removeVehicle(Vehicle vehicle) {
        World world = worldRepository.findOne(vehicle.getWorld().getId());
        if (Objects.nonNull(world)) {
            world.setVehicles(world.getVehicles().stream().filter(v -> !v.getId().equals(vehicle.getId())).collect(Collectors.toSet()));
            worldRepository.save(world);
        }
    }

    public World addStop(World world, Cell cell) {
        Cell originalCell = world.getCells()[cell.getX()][cell.getY()];
        if (CellType.STOP == originalCell.getType()) {
            return world;
        }
        originalCell.setType(CellType.STOP);
        originalCell.setName(RandomStopsUtils.generate());
        return worldRepository.save(world);
    }

    public World addBuilding(World world, Cell cell) {
        Cell originalCell = world.getCells()[cell.getX()][cell.getY()];
        if (CellType.BUILDING == originalCell.getType()) {
            return world;
        }
        originalCell.setType(CellType.BUILDING);
        originalCell.setName(RandomBuildingsUtils.generate());
        return worldRepository.save(world);
    }

    public World clear(World world, Cell cell) {
        Cell originalCell = world.getCells()[cell.getX()][cell.getY()];
        if (CellType.DESERT == originalCell.getType() || CellType.WATER == originalCell.getType() || CellType.TREE == originalCell.getType()) {
            return world;
        }

        originalCell.setType(getNewType(world, cell, 1));
        originalCell.setName(null);
        return worldRepository.save(world);
    }

    private CellType getNewType(World world, Cell cell, int threshold) {
        final int x = cell.getX();
        final int y = cell.getY();
        final Cell[][] cells = world.getCells();
        final CellType originalType = cells[x][y].getType();

        if (threshold > world.getMeasure()) {
            return CellType.DESERT;
        }

        if (CellType.BUILDING == originalType) {
            return CellType.DESERT;
        }

        if (CellType.STOP == originalType) {
            int water = 0;
            int desert = 0;
            for (int i = x - threshold; i <= x + threshold; i++) {
                if (i < 0 || world.getMeasure() <= i) continue;
                for (int j = y - threshold; j <= y + threshold; j++) {
                    if (j < 0 || world.getMeasure() <= j) continue;
                    CellType type = cells[i][j].getType();
                    switch (type) {
                        case BUILDING:
                        case DESERT:
                        case TREE:
                            desert++;
                            break;
                        case WATER:
                            water++;
                            break;
                    }
                }
            }
            if (desert > 0 || water > 0) {
                return desert > water ? CellType.DESERT : CellType.WATER;
            } else return getNewType(world, cell, threshold++);
        }

        return originalType;
    }
}
