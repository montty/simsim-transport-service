package vnd.simsimtransport.model.world;

import java.awt.*;

public enum CellType {
    DESERT(Color.decode("#e4b152")), STOP(Color.decode("#ce64ce")), TREE(Color.decode("#45a045")), WATER(Color.decode("#104dbe")), BUILDING(Color.decode("#614747")), SUBWAY(Color.black);

    private Color color;

    CellType(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public static CellType fromColor(int color) {
        for (CellType cellType : CellType.values()) {
            if (color == cellType.getColor().getRGB()) {
                return cellType;
            }
        }
        return null;
    }
}
