package vnd.simsimtransport.model.world;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QWorld is a Querydsl query type for World
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QWorld extends EntityPathBase<World> {

    private static final long serialVersionUID = 237450730L;

    public static final QWorld world = new QWorld("world");

    public final ArrayPath<Cell[][], Cell[]> cells = createArray("cells", Cell[][].class);

    public final DateTimePath<java.util.Date> created = createDateTime("created", java.util.Date.class);

    public final StringPath creator = createString("creator");

    public final StringPath id = createString("id");

    public final NumberPath<Integer> measure = createNumber("measure", Integer.class);

    public final StringPath name = createString("name");

    public final SetPath<vnd.simsimtransport.model.vehicle.Vehicle, vnd.simsimtransport.model.vehicle.QVehicle> vehicles = this.<vnd.simsimtransport.model.vehicle.Vehicle, vnd.simsimtransport.model.vehicle.QVehicle>createSet("vehicles", vnd.simsimtransport.model.vehicle.Vehicle.class, vnd.simsimtransport.model.vehicle.QVehicle.class, PathInits.DIRECT2);

    public QWorld(String variable) {
        super(World.class, forVariable(variable));
    }

    public QWorld(Path<? extends World> path) {
        super(path.getType(), path.getMetadata());
    }

    public QWorld(PathMetadata<?> metadata) {
        super(World.class, metadata);
    }

}

