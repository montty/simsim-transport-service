package vnd.simsimtransport.model.world;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QCell is a Querydsl query type for Cell
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QCell extends BeanPath<Cell> {

    private static final long serialVersionUID = 284148746L;

    public static final QCell cell = new QCell("cell");

    public final StringPath name = createString("name");

    public final EnumPath<CellType> type = createEnum("type", CellType.class);

    public final BooleanPath underground = createBoolean("underground");

    public final NumberPath<Integer> x = createNumber("x", Integer.class);

    public final NumberPath<Integer> y = createNumber("y", Integer.class);

    public QCell(String variable) {
        super(Cell.class, forVariable(variable));
    }

    public QCell(Path<? extends Cell> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCell(PathMetadata<?> metadata) {
        super(Cell.class, metadata);
    }

}

