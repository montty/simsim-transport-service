package vnd.simsimtransport.model.world;

import com.mysema.query.annotations.QuerySupertype;
import vnd.simsimtransport.model.vehicle.Vehicle;

import javax.persistence.Id;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@QuerySupertype
@org.springframework.data.mongodb.core.mapping.Document
public class World {

    @Id
    private String id;
    private String creator;
    private Date created;
    private String name;
    private int measure;
    private Cell[][] cells;
    private Set<Vehicle> vehicles;

    public World() {
    }

    public int getMeasure() {
        return measure;
    }

    public void setMeasure(int measure) {
        this.measure = measure;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Vehicle> getVehicles() {
        if (Objects.isNull(vehicles)) {
            return Collections.emptySet();
        }
        return vehicles;
    }

    public void setVehicles(Set<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public World cutDownVersion() {
        World world = new World();
        world.setId(id);
        world.setName(name);
        world.setMeasure(measure);
        return world;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        World world = (World) o;
        return Objects.equals(id, world.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
