package vnd.simsimtransport.model.world;

import com.mysema.query.types.path.StringPath;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

/**
 * World repository.
 */
public interface WorldRepository extends MongoRepository<World, String>, QueryDslPredicateExecutor<World>, QuerydslBinderCustomizer<QWorld> {

    World findAllByCreatorOrderByCreatedDesc(String creator);

    @Override
    default void customize(QuerydslBindings querydslBindings, QWorld root) {
        querydslBindings.bind(String.class).first((StringPath path, String value) -> path.containsIgnoreCase(value));
    }

}
