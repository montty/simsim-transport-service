package vnd.simsimtransport.model;

import com.mysema.query.annotations.QuerySupertype;
import vnd.simsimtransport.model.settings.Settings;
import vnd.simsimtransport.utils.RandomUtils;

import javax.persistence.Id;
import java.util.Objects;

@QuerySupertype
@org.springframework.data.mongodb.core.mapping.Document
public class UserData {
    @Id
    private String id;
    private Role role;
    private String login;
    private String name;
    private Settings settings;

    public UserData() {
    }

    public UserData(String id, Role role, String login) {
        this.id = id;
        this.role = role;
        this.login = login;
        this.name = RandomUtils.nextName(" ");
        this.settings = Settings.DEFAULT;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Settings getSettings() {
        if (Objects.isNull(settings)) {
            return Settings.DEFAULT;
        }
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }
}
