package vnd.simsimtransport.model;

import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.StringPath;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

/**
 * Account data repository.
 */
//@RepositoryRestResource
public interface CredentialsRepository extends MongoRepository<Credentials, String>, QueryDslPredicateExecutor<Credentials>, QuerydslBinderCustomizer<QCredentials> {

    Credentials findOne(Predicate predicate);
    Credentials findOneByLogin(String login);

    @Override
    default void customize(QuerydslBindings querydslBindings, QCredentials root) {
        querydslBindings.bind(String.class).first((StringPath path, String value) -> path.containsIgnoreCase(value));
    }
}
