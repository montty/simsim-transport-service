package vnd.simsimtransport.model.vehicle;

import com.mysema.query.annotations.QuerySupertype;
import vnd.simsimtransport.model.world.World;

import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

@QuerySupertype
@org.springframework.data.mongodb.core.mapping.Document
public class Vehicle {
    @Id
    private String id;
    private char symbol;
    private String color;
    private WayType wayType;
    private String name;
    private String creator;
    private Date created;
    private short hue;
    private World world;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public WayType getWayType() {
        return wayType;
    }

    public void setWayType(WayType wayType) {
        this.wayType = wayType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public short getHue() {
        return hue;
    }

    public void setHue(short hue) {
        this.hue = hue;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return Objects.equals(id, vehicle.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
