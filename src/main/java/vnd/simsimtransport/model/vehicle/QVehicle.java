package vnd.simsimtransport.model.vehicle;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QVehicle is a Querydsl query type for Vehicle
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QVehicle extends EntityPathBase<Vehicle> {

    private static final long serialVersionUID = -2123544546L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QVehicle vehicle = new QVehicle("vehicle");

    public final StringPath color = createString("color");

    public final DateTimePath<java.util.Date> created = createDateTime("created", java.util.Date.class);

    public final StringPath creator = createString("creator");

    public final NumberPath<Short> hue = createNumber("hue", Short.class);

    public final StringPath id = createString("id");

    public final StringPath name = createString("name");

    public final ComparablePath<Character> symbol = createComparable("symbol", Character.class);

    public final EnumPath<WayType> wayType = createEnum("wayType", WayType.class);

    public final vnd.simsimtransport.model.world.QWorld world;

    public QVehicle(String variable) {
        this(Vehicle.class, forVariable(variable), INITS);
    }

    public QVehicle(Path<? extends Vehicle> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QVehicle(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QVehicle(PathMetadata<?> metadata, PathInits inits) {
        this(Vehicle.class, metadata, inits);
    }

    public QVehicle(Class<? extends Vehicle> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.world = inits.isInitialized("world") ? new vnd.simsimtransport.model.world.QWorld(forProperty("world")) : null;
    }

}

