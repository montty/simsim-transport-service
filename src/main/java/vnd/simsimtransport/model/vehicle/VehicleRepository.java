package vnd.simsimtransport.model.vehicle;

import com.mysema.query.types.path.StringPath;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

/**
 * vehicle repository.
 */
public interface VehicleRepository extends MongoRepository<Vehicle, String>, QueryDslPredicateExecutor<Vehicle>, QuerydslBinderCustomizer<QVehicle> {

    @Override
    default void customize(QuerydslBindings querydslBindings, QVehicle root) {
        querydslBindings.bind(String.class).first((StringPath path, String value) -> path.containsIgnoreCase(value));
    }

}
