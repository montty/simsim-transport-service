package vnd.simsimtransport.model.vehicle;

public enum WayType {
    WATER("83ffb8"),          // only water cells
    AIR("83d5ff"),            // like queen
    GROUND("ffdd83"),         // avoid trees and water
    UNDERGROUND("ff83bd");     // only subway cells

    private String color;

    WayType(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}
