package vnd.simsimtransport.model.vehicle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vnd.simsimtransport.utils.RandomVehiclesUtils;

import java.util.Date;
import java.util.Random;

@Service
public class VehicleService {

    private final VehicleRepository vehicleRepository;

    @Autowired
    public VehicleService(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    public Vehicle generate(String userId) {
        Vehicle vehicle = new Vehicle();
        vehicle.setCreator(userId);
        vehicle.setCreated(new Date());
        Random random = new Random();
        vehicle.setWayType(WayType.values()[random.nextInt(WayType.values().length)]);
        vehicle.setName(RandomVehiclesUtils.generate(vehicle.getWayType()));
        vehicle.setColor("#" + vehicle.getWayType().getColor());
        String symbols = "♦♥♠♣☼◊♫♪▲αβγδεζθλμνπωφτσρΞ";
        vehicle.setSymbol(symbols.charAt(random.nextInt(symbols.length())));
        vehicle.setHue((short) random.nextInt(360));
        vehicleRepository.save(vehicle);
        return vehicle;
    }

}
