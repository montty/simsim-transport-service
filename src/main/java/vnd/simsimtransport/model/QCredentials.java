package vnd.simsimtransport.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QCredentials is a Querydsl query type for Credentials
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QCredentials extends EntityPathBase<Credentials> {

    private static final long serialVersionUID = -1984019792L;

    public static final QCredentials credentials = new QCredentials("credentials");

    public final StringPath id = createString("id");

    public final StringPath login = createString("login");

    public final StringPath password = createString("password");

    public QCredentials(String variable) {
        super(Credentials.class, forVariable(variable));
    }

    public QCredentials(Path<? extends Credentials> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCredentials(PathMetadata<?> metadata) {
        super(Credentials.class, metadata);
    }

}

