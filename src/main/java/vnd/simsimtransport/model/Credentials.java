package vnd.simsimtransport.model;

import com.mysema.query.annotations.QuerySupertype;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Id;

@QuerySupertype
@org.springframework.data.mongodb.core.mapping.Document
public class Credentials {
    @Id
    private String id;
    @NotBlank
    private String login;
    @NotBlank
    private String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
