package vnd.simsimtransport.model.vehicle;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import vnd.simsimtransport.App;

import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@WebAppConfiguration
public class VehicleServiceTest {


    @Autowired
    private VehicleService vehicleService;

    @Test
    public void generateTest() throws IOException {
        Vehicle vehicle = vehicleService.generate("X");
        Assert.assertNotNull(vehicle.getId());
    }

}