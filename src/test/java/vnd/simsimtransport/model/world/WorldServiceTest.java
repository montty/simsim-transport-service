package vnd.simsimtransport.model.world;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import vnd.simsimtransport.App;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@WebAppConfiguration
public class WorldServiceTest {

    @Autowired
    private WorldService worldService;

    @Test
    public void generateGroundImageTest() throws IOException {
        BufferedImage image = worldService.generateGroundImage(100, 30, 20, 10, 2);
        File file = new File("world.png");
        ImageIO.write(image, "png", file);
        Assert.assertTrue(file.length() > 0);
        System.out.println(String.format("World wile saved to %s", file.getAbsolutePath()));
    }

    @Test
    public void generateSubwayImageTest() throws IOException {
        BufferedImage image = worldService.generateSubwayImage(100);
        File file = new File("subway.png");
        ImageIO.write(image, "png", file);
        Assert.assertTrue(file.length() > 0);
        System.out.println(String.format("Subway file saved to %s", file.getAbsolutePath()));
    }

    @Test
    public void generateWorldTest() throws IOException {
        World world = worldService.generate(10, "!");
        Assert.assertEquals(10, world.getMeasure());
    }

    @Test
    public void generateWorldImage() throws IOException {
        World world = worldService.generate(256, "!");
        byte[] image = WorldService.getImage(world);
        File file = new File("world_full_image.png");
        FileUtils.writeByteArrayToFile(file, image);
        Assert.assertTrue(file.length() > 0);
        System.out.println(String.format("World full image file saved to %s", file.getAbsolutePath()));
    }
}