import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import vnd.simsimtransport.App;
import vnd.simsimtransport.model.*;
import vnd.simsimtransport.service.IMainService;
import vnd.simsimtransport.utils.DataUtils;
import vnd.simsimtransport.utils.RandomUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@WebAppConfiguration
public class MainTest {

    private static final Logger log = LoggerFactory.getLogger("test");

    @Autowired
    IMainService mainService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MessageRepository messageRepository;
    @Autowired
    CredentialsRepository credentialsRepository;
    @Autowired
    UserDataRepository userDataRepository;

    @Test
    public void createUsersTest() {
        Credentials credentials = new Credentials();
        credentials.setLogin(RandomUtils.randomLogin());
        credentials.setPassword(DataUtils.hashedPassword(credentials.getLogin()));
        credentialsRepository.save(credentials);
        UserData userData = new UserData(credentials.getId(), Role.MANAGER, credentials.getLogin());
        userDataRepository.save(userData);
        System.out.println("Created: " + credentials.getLogin());
        Assert.assertNotNull(userDataRepository.findOne(credentialsRepository.findOneByLogin(credentials.getLogin()).getId()));
    }
//
//    @Test
//    public void objectMapperTest() throws JsonProcessingException {
//        PaymentRequestOrderWithInvoice paymentRequestOrderWithInvoice = new PaymentRequestOrderWithInvoice(new InvoiceRequest());
//        paymentRequestOrderWithInvoice.setSender("fyvaao@ya.ru");
//        String id = new ObjectId().toHexString();
//        paymentRequestOrderWithInvoice.setRequestId(id);
//        System.err.println(objectMapper.writeValueAsString(new PaymentRequestOrderWithInvoiceMessage(paymentRequestOrderWithInvoice)));
//    }
}

