import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import vnd.simsimtransport.App;
import vnd.simsimtransport.utils.DataUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@WebAppConfiguration
public class CommonTest {

    @Test
    @Ignore
    public void testDate() {
        Assert.assertFalse(DataUtils.isDateValid("29.02.2015"));
        Assert.assertTrue(DataUtils.isDateValid("29.02.2016"));
    }
}

